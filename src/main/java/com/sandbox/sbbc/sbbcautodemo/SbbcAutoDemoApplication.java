package com.sandbox.sbbc.sbbcautodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbbcAutoDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbbcAutoDemoApplication.class, args);
    }
}
