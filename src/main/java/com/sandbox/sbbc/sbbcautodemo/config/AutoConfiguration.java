package com.sandbox.sbbc.sbbcautodemo.config;

import com.sandbox.sbbc.sbbcautodemo.datasource.SBBCdataSource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AutoConfiguration {

    @ConditionalOnMissingBean
    @Bean
    public SBBCdataSource getDefaultSBBCdataSource() {
        SBBCdataSource defaultDataSource = new SBBCdataSource();
        defaultDataSource.setConnectionString("default connection");
        defaultDataSource.setResourceName("default resource");
        return defaultDataSource;
    }
}
