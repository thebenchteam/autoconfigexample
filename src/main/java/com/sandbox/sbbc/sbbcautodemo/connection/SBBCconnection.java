package com.sandbox.sbbc.sbbcautodemo.connection;

import com.sandbox.sbbc.sbbcautodemo.datasource.SBBCdataSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class SBBCconnection {

    private SBBCdataSource dataSource;

    public SBBCconnection(SBBCdataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public String toString() {
        return "SBBCconnection{" +
                "dataSource=" + dataSource +
                '}';
    }
}
