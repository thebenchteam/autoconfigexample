package com.sandbox.sbbc.sbbcautodemo.datasource;

public class SBBCdataSource {

    private String connectionString;
    private String resourceName;

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    @Override
    public String toString() {
        return "SBBCdataSource{" +
                "connectionString='" + connectionString + '\'' +
                ", resourceName='" + resourceName + '\'' +
                '}';
    }
}
